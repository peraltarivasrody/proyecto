function validarFormulario() {
  var nombres = document.getElementById("nombres");
  var apellidos = document.getElementById("apellidos");
  var correo = document.getElementById("correo");
  var telefono = document.getElementById("telefono");
  var direccion = document.getElementById("direccion");

  var mensajesError = "";

  if (nombres.value == "") {
    mensajesError += "Ingrese sus nombres\n";
  }
  if (apellidos.value == "") {
    mensajesError += "Ingrese sus apellidos\n";
  }
  if (correo.value == "") {
    mensajesError += "Ingrese su correo\n";
  } else {
    var patronCorreo = /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/;
    if (!patronCorreo.test(correo.value)) {
      mensajesError += "Ingrese un correo electrónico válido\n";
    }
  }
  if (telefono.value == "") {
    mensajesError += "Ingrese su número de teléfono\n";
  } else {
    var patronNumeros = /^\d+$/;
    if (!patronNumeros.test(telefono.value) || telefono.value.length !== 10) {
      mensajesError += "Ingrese un número de teléfono válido (10 dígitos)\n";
    }
  }
  if (direccion.value == "") {
    mensajesError += "Ingrese su dirección\n";
  }

  if (mensajesError !== "") {
    alert(mensajesError);
    return false;
  }
  return true;
}

  
  